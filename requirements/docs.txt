sphinx>=5.1.1
furo>=2022.6.21
sphinx-copybutton>=0.5.0
Sphinx-Substitution-Extensions>=2022.2.16
sphinx-notfound-page>=0.8.3
