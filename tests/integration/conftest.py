import asyncio
import sys
import unittest.mock as mock
from typing import Any
from typing import Dict
from typing import List

import dict_tools.data
import pop.hub
import pytest
import pytest_asyncio


# ================================================================================
# idem-aws fixtures
# ================================================================================
@pytest.fixture(scope="module")
def acct_data(ctx):
    """
    acct_data that can be used in running simple yaml blocks
    """
    yield {"profiles": {"aws": {"default": ctx.acct}}}


# ================================================================================
# pop fixtures
# ================================================================================
@pytest.fixture(scope="module", autouse=True)
def acct_subs() -> List[str]:
    return ["aws"]


@pytest.fixture(scope="module", autouse=True)
def acct_profile() -> str:
    return "test_development_idem_aws"


@pytest.fixture(scope="module")
def event_loop():
    loop = asyncio.get_event_loop()
    yield loop
    loop.close()


@pytest.fixture(scope="module", name="hub")
def tpath_hub(code_dir, event_loop):
    """
    Add "idem_plugin" to the test path
    """
    TPATH_DIR = str(code_dir / "tests" / "tpath")

    with mock.patch("sys.path", [TPATH_DIR] + sys.path):
        hub = pop.hub.Hub()
        hub.pop.loop.CURRENT_LOOP = event_loop
        hub.pop.sub.add(dyne_name="idem")
        hub.pop.config.load(["idem", "acct"], "idem", parse_cli=False)
        hub.idem.RUNS = {"test": {}}

        yield hub


@pytest_asyncio.fixture(scope="module", name="ctx")
async def integration_ctx(
    hub, acct_subs: List[str], acct_profile: str
) -> Dict[str, Any]:
    ctx = dict_tools.data.NamespaceDict(
        run_name="test",
        test=False,
        tag="fake_|-test_|-tag",
        old_state={},
    )

    # Add the profile to the account
    if hub.OPT.acct.acct_file:
        await hub.acct.init.unlock(hub.OPT.acct.acct_file, hub.OPT.acct.acct_key)
        ctx.acct = await hub.acct.init.gather(acct_subs, acct_profile)
    else:
        hub.log.warning("No credentials found, assuming localstack on localhost")
        ctx.acct = dict(
            region_name="us-west-2",
            endpoint_url="http://0.0.0.0:4566",
            aws_access_key_id="localstack",
            aws_secret_access_key="localstack",
        )

    yield ctx


# --------------------------------------------------------------------------------
