import copy
import time
from collections import ChainMap

import pytest
from flaky import flaky

STATE_NAME = "aws.s3.bucket_lifecycle"


@pytest.mark.flaky(max_runs=5)
@pytest.mark.localstack(pro=True)
@pytest.mark.asyncio
async def test_bucket_lifecycle(hub, ctx, aws_s3_bucket):
    bucket_lifecycle_temp_name = "idem-test-bucket-lifecycle-" + str(int(time.time()))
    bucket = aws_s3_bucket.get("name")
    lifecycle_configuration = {
        "Rules": [
            {
                "ID": "rule-1",
                "Filter": {},
                "Status": "Enabled",
                "NoncurrentVersionTransitions": [
                    {
                        "NoncurrentDays": 31,
                        "StorageClass": "STANDARD_IA",
                        "NewerNoncurrentVersions": 31,
                    }
                ],
                "NoncurrentVersionExpiration": {
                    "NoncurrentDays": 60,
                    "NewerNoncurrentVersions": 60,
                },
                "AbortIncompleteMultipartUpload": {"DaysAfterInitiation": 5},
            }
        ]
    }
    new_lifecycle_configuration = {
        "Rules": [
            {
                "ID": "rule-2",
                "Filter": {},
                "Status": "Enabled",
                "NoncurrentVersionTransitions": [
                    {
                        "NoncurrentDays": 60,
                        "StorageClass": "GLACIER",
                        "NewerNoncurrentVersions": 60,
                    }
                ],
                "NoncurrentVersionExpiration": {
                    "NoncurrentDays": 90,
                    "NewerNoncurrentVersions": 90,
                },
                "AbortIncompleteMultipartUpload": {"DaysAfterInitiation": 7},
            }
        ]
    }

    test_ctx = copy.deepcopy(ctx)
    test_ctx["test"] = True

    # Create bucket lifecycle with test flag
    ret = await hub.states.aws.s3.bucket_lifecycle.present(
        test_ctx,
        name=bucket_lifecycle_temp_name,
        bucket=bucket,
        lifecycle_configuration=lifecycle_configuration,
    )

    assert ret["result"], ret["comment"]
    assert (
        hub.tool.aws.comment_utils.would_create_comment(
            resource_type=STATE_NAME, name=bucket_lifecycle_temp_name
        )[0]
        in ret["comment"]
    )
    assert not ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert bucket_lifecycle_temp_name == resource.get("name")
    assert bucket == resource.get("bucket")
    assert lifecycle_configuration == resource.get("lifecycle_configuration")

    # Create bucket lifecycle
    ret = await hub.states.aws.s3.bucket_lifecycle.present(
        ctx,
        name=bucket_lifecycle_temp_name,
        bucket=bucket,
        lifecycle_configuration=lifecycle_configuration,
    )

    assert ret["result"], ret["comment"]
    assert (
        hub.tool.aws.comment_utils.create_comment(
            resource_type=STATE_NAME, name=bucket_lifecycle_temp_name
        )[0]
        in ret["comment"]
    )
    assert not ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert bucket_lifecycle_temp_name == resource.get("name")
    assert bucket == resource.get("resource_id")
    assert bucket == resource.get("bucket")
    assert lifecycle_configuration == resource.get("lifecycle_configuration")

    resource_id = resource.get("resource_id")

    # Verify that the created bucket lifecycle is present (describe)
    ret = await hub.states.aws.s3.bucket_lifecycle.describe(ctx)

    resource_name = f"{resource_id}-lifecycle"
    assert resource_name in ret
    assert f"{STATE_NAME}.present" in ret.get(resource_name)
    resource = ret.get(resource_name).get(f"{STATE_NAME}.present")
    resource_map = dict(ChainMap(*resource))
    assert resource_name == resource_map.get("name")
    assert resource_id == resource_map.get("resource_id")
    assert bucket == resource_map.get("bucket")
    assert lifecycle_configuration == resource_map.get("lifecycle_configuration")

    # Create bucket lifecycle again with different bucket name and resource_id with test flag
    ret = await hub.states.aws.s3.bucket_lifecycle.present(
        test_ctx,
        name=bucket_lifecycle_temp_name,
        bucket=bucket,
        lifecycle_configuration=lifecycle_configuration,
        resource_id=bucket_lifecycle_temp_name,
    )

    assert not ret["result"] and ret["comment"]
    assert (
        f"Bucket '{bucket}' and resource_id '{bucket_lifecycle_temp_name}' parameters must be the same"
        in ret["comment"]
    )
    assert not ret.get("old_state") and not ret.get("new_state")

    # Create bucket lifecycle again with different bucket name and resource_id
    ret = await hub.states.aws.s3.bucket_lifecycle.present(
        ctx,
        name=bucket_lifecycle_temp_name,
        bucket=bucket,
        lifecycle_configuration=lifecycle_configuration,
        resource_id=bucket_lifecycle_temp_name,
    )

    assert not ret["result"] and ret["comment"]
    assert (
        f"Bucket '{bucket}' and resource_id '{bucket_lifecycle_temp_name}' parameters must be the same"
        in ret["comment"]
    )
    assert not ret.get("old_state") and not ret.get("new_state")

    # Create bucket lifecycle again with same resource_id and no change in state with test flag
    ret = await hub.states.aws.s3.bucket_lifecycle.present(
        test_ctx,
        name=bucket_lifecycle_temp_name,
        bucket=bucket,
        lifecycle_configuration=lifecycle_configuration,
        resource_id=resource_id,
    )

    assert ret["result"], ret["comment"]
    assert (
        hub.tool.aws.comment_utils.already_exists_comment(
            resource_type=STATE_NAME,
            name=bucket_lifecycle_temp_name,
        )[0]
        in ret["comment"]
    )
    assert ret.get("old_state") and ret.get("new_state")

    # Create bucket lifecycle again with same resource_id and no change in state
    ret = await hub.states.aws.s3.bucket_lifecycle.present(
        ctx,
        name=bucket_lifecycle_temp_name,
        bucket=bucket,
        lifecycle_configuration=lifecycle_configuration,
        resource_id=resource_id,
    )

    assert ret["result"], ret["comment"]
    assert (
        hub.tool.aws.comment_utils.already_exists_comment(
            resource_type=STATE_NAME,
            name=bucket_lifecycle_temp_name,
        )[0]
        in ret["comment"]
    )
    assert ret.get("old_state") and ret.get("new_state")

    # Update bucket lifecycle with test flag
    ret = await hub.states.aws.s3.bucket_lifecycle.present(
        test_ctx,
        name=bucket_lifecycle_temp_name,
        bucket=bucket,
        lifecycle_configuration=new_lifecycle_configuration,
        resource_id=resource_id,
    )

    assert ret["result"], ret["comment"]
    assert (
        hub.tool.aws.comment_utils.already_exists_comment(
            resource_type=STATE_NAME,
            name=bucket_lifecycle_temp_name,
        )[0]
        in ret["comment"]
    )
    assert (
        hub.tool.aws.comment_utils.would_update_comment(
            resource_type=STATE_NAME, name=bucket_lifecycle_temp_name
        )[0]
        in ret["comment"]
    )
    assert ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert bucket_lifecycle_temp_name == resource.get("name")
    assert bucket_lifecycle_temp_name == resource.get("name")
    assert bucket == resource.get("bucket")
    assert new_lifecycle_configuration == resource.get("lifecycle_configuration")

    # Update bucket lifecycle
    ret = await hub.states.aws.s3.bucket_lifecycle.present(
        ctx,
        name=bucket_lifecycle_temp_name,
        bucket=bucket,
        lifecycle_configuration=new_lifecycle_configuration,
        resource_id=resource_id,
    )

    assert ret["result"], ret["comment"]
    assert (
        hub.tool.aws.comment_utils.already_exists_comment(
            resource_type=STATE_NAME,
            name=bucket_lifecycle_temp_name,
        )[0]
        in ret["comment"]
    )
    assert (
        hub.tool.aws.comment_utils.update_comment(
            resource_type=STATE_NAME, name=bucket_lifecycle_temp_name
        )[0]
        in ret["comment"]
    )
    assert ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert bucket_lifecycle_temp_name == resource.get("name")
    assert bucket == resource.get("bucket")
    assert new_lifecycle_configuration == resource.get("lifecycle_configuration")

    # Delete bucket lifecycle with test flag
    ret = await hub.states.aws.s3.bucket_lifecycle.absent(
        test_ctx,
        name=bucket_lifecycle_temp_name,
        resource_id=resource_id,
    )

    assert ret["result"], ret["comment"]
    assert (
        hub.tool.aws.comment_utils.would_delete_comment(
            resource_type=STATE_NAME, name=bucket_lifecycle_temp_name
        )[0]
        in ret["comment"]
    )
    assert ret.get("old_state") and not ret.get("new_state")
    resource = ret.get("old_state")
    assert bucket_lifecycle_temp_name == resource.get("name")
    assert bucket == resource.get("bucket")

    # Delete bucket lifecycle
    ret = await hub.states.aws.s3.bucket_lifecycle.absent(
        ctx,
        name=bucket_lifecycle_temp_name,
        resource_id=resource_id,
    )

    assert ret["result"], ret["comment"]
    assert (
        hub.tool.aws.comment_utils.delete_comment(
            resource_type=STATE_NAME, name=bucket_lifecycle_temp_name
        )[0]
        in ret["comment"]
    )
    assert ret.get("old_state") and not ret.get("new_state")
    resource = ret.get("old_state")
    assert bucket_lifecycle_temp_name == resource.get("name")
    assert bucket == resource.get("bucket")

    # Delete the same bucket lifecycle again (deleted state) will not invoke delete on AWS side
    ret = await hub.states.aws.s3.bucket_lifecycle.absent(
        ctx,
        name=bucket_lifecycle_temp_name,
        resource_id=resource_id,
    )

    assert ret["result"], ret["comment"]
    assert (
        hub.tool.aws.comment_utils.already_absent_comment(
            resource_type=STATE_NAME, name=bucket_lifecycle_temp_name
        )[0]
        in ret["comment"]
    )
    assert (not ret["old_state"]) and (not ret["new_state"])

    # Delete bucket lifecycle with no resource_id will consider it as absent
    ret = await hub.states.aws.s3.bucket_lifecycle.absent(
        ctx, name=bucket_lifecycle_temp_name
    )

    assert ret["result"], ret["comment"]
    assert (
        hub.tool.aws.comment_utils.already_absent_comment(
            resource_type=STATE_NAME, name=bucket_lifecycle_temp_name
        )[0]
        in ret["comment"]
    )
    assert (not ret["old_state"]) and (not ret["new_state"])
