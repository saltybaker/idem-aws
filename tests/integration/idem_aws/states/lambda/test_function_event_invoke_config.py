import copy
import time
from collections import ChainMap
from typing import List

import pytest
import pytest_asyncio

PARAMETRIZE = dict(argnames="__test", argvalues=[True, False], ids=["--test", "run"])
NAME = "idem-test-function-event-config-" + str(int(time.time()))
PARAMETER = {
    "name": NAME,
    "maximum_retry_attempts": 1,
    "maximum_event_age_in_seconds": 2000,
    "qualifier": "$LATEST",
}
comment_utils_kwargs = {
    "resource_type": "aws.lambda.function_event_invoke_config",
    "name": NAME,
}


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(name="present")
async def test_present(hub, ctx, __test, aws_lambda_function, cleanup):
    global PARAMETER
    ctx["test"] = __test
    function_name = aws_lambda_function["name"]
    PARAMETER["function_name"] = function_name

    would_create_message = hub.tool.aws.comment_utils.would_create_comment(
        **comment_utils_kwargs
    )[0]
    created_message = hub.tool.aws.comment_utils.create_comment(**comment_utils_kwargs)[
        0
    ]

    # Create with test flag set to true
    ret = await hub.states.aws.lambda_aws.function_event_invoke_config.present(
        ctx,
        **PARAMETER,
    )
    assert ret["result"], ret["comment"]
    resource = ret.get("new_state")

    if __test:
        assert would_create_message in ret["comment"]
    else:
        resource_id = resource.get("resource_id")
        # resource_id is the function ARN and ARN returned by localstack does not include the qualifier.
        # This is different from the ARN returned from AWS and breaks the expected format
        # defined in hub.tool.aws.arn_utils. Hence, we have to add the qualifier if test is running against localstack.
        if hub.tool.utils.is_running_localstack(ctx):
            resource_id += ":$LATEST"
        PARAMETER["resource_id"] = resource_id
        assert created_message in ret["comment"]

    assert not ret.get("old_state") and ret.get("new_state")
    assert PARAMETER["name"] == resource.get("name")
    assert PARAMETER["maximum_retry_attempts"] == resource.get("maximum_retry_attempts")
    assert PARAMETER["maximum_event_age_in_seconds"] == resource.get(
        "maximum_event_age_in_seconds"
    )


@pytest.mark.asyncio
@pytest.mark.localstack(False)
@pytest.mark.dependency(name="describe", depends=["present"])
async def test_describe(hub, ctx):
    # localstack does not support listing the configurations, hence skipping if running on localstack
    describe_ret = (
        await hub.states.aws.lambda_aws.function_event_invoke_config.describe(ctx)
    )
    resource_id = PARAMETER["resource_id"]
    assert resource_id in describe_ret
    resource = dict(
        ChainMap(
            *describe_ret[resource_id].get(
                "aws.lambda.function_event_invoke_config.present"
            )
        )
    )

    # function ARN is used as the resource name during describe. lambda function name is part of the function ARN
    # assert if name contains the lambda function name
    assert PARAMETER["function_name"] in resource.get("name")
    assert PARAMETER["maximum_retry_attempts"] == resource.get("maximum_retry_attempts")
    assert PARAMETER["maximum_event_age_in_seconds"] == resource.get(
        "maximum_event_age_in_seconds"
    )


@pytest.mark.asyncio
@pytest.mark.dependency(name="exec-get", depends=["present"])
async def test_exec_get(hub, ctx):
    ret = await hub.exec.aws.lambda_aws.function_event_invoke_config.get(
        ctx,
        name=PARAMETER["name"],
        resource_id=PARAMETER["resource_id"],
    )

    assert ret["result"], ret["comment"]
    assert ret["ret"]
    resource = ret["ret"]
    assert PARAMETER["name"] == resource.get("name")
    assert PARAMETER["function_name"] == resource.get("function_name")


@pytest.mark.asyncio
@pytest.mark.localstack(False)
@pytest.mark.dependency(name="exec-list", depends=["present"])
async def test_exec_list(hub, ctx):
    # localstack does not support listing the configurations, hence skipping if running on localstack
    list_ret = await hub.exec.aws.lambda_aws.function_event_invoke_config.list(
        ctx,
        function_name=PARAMETER["function_name"],
    )

    assert list_ret and list_ret["result"] and list_ret["ret"]
    assert isinstance(list_ret["ret"], List)
    assert len(list_ret["ret"]) == 1
    result = list_ret["ret"][0]
    assert PARAMETER["function_name"] == result.get("function_name")
    assert PARAMETER["function_name"] in result.get("name")
    assert PARAMETER["maximum_retry_attempts"] == result.get("maximum_retry_attempts")
    assert PARAMETER["maximum_event_age_in_seconds"] == result.get(
        "maximum_event_age_in_seconds"
    )


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(name="update", depends=["present"])
async def test_update(hub, ctx, __test):
    global PARAMETER
    new_parameter = copy.deepcopy(PARAMETER)
    ctx["test"] = __test
    new_parameter["maximum_retry_attempts"] = 2
    new_parameter["maximum_event_age_in_seconds"] = 2600
    would_update_message = hub.tool.aws.comment_utils.would_update_comment(
        **comment_utils_kwargs
    )[0]
    update_message = hub.tool.aws.comment_utils.update_comment(**comment_utils_kwargs)[
        0
    ]

    ret = await hub.states.aws.lambda_aws.function_event_invoke_config.present(
        ctx, **new_parameter
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    if __test:
        assert would_update_message in ret["comment"]
    else:
        assert update_message in ret["comment"]

    old_resource = ret["old_state"]
    assert PARAMETER["name"] == old_resource.get("name")
    assert PARAMETER["maximum_retry_attempts"] == old_resource.get(
        "maximum_retry_attempts"
    )
    assert PARAMETER["maximum_event_age_in_seconds"] == old_resource.get(
        "maximum_event_age_in_seconds"
    )

    new_resource = ret.get("new_state")
    assert PARAMETER["name"] == new_resource.get("name")
    assert new_parameter["maximum_retry_attempts"] == new_resource.get(
        "maximum_retry_attempts"
    )
    assert new_parameter["maximum_event_age_in_seconds"] == new_resource.get(
        "maximum_event_age_in_seconds"
    )


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(name="absent", depends=["update"])
async def test_absent(hub, ctx, __test):
    global PARAMETER
    ctx["test"] = __test
    deleted_message = hub.tool.aws.comment_utils.delete_comment(**comment_utils_kwargs)[
        0
    ]
    would_delete_message = hub.tool.aws.comment_utils.would_delete_comment(
        **comment_utils_kwargs
    )[0]

    ret = await hub.states.aws.lambda_aws.function_event_invoke_config.absent(
        ctx,
        name=PARAMETER["name"],
        resource_id=PARAMETER["resource_id"],
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")
    if __test:
        assert would_delete_message in ret["comment"]
    else:
        assert deleted_message in ret["comment"]

    old_resource = ret.get("old_state")
    assert PARAMETER["name"] == old_resource.get("name")


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(name="already_absent", depends=["absent"])
async def test_already_absent(hub, ctx, __test):
    global PARAMETER
    ctx["test"] = __test
    already_absent_message = hub.tool.aws.comment_utils.already_absent_comment(
        **comment_utils_kwargs
    )[0]

    ret = await hub.states.aws.lambda_aws.function_event_invoke_config.absent(
        ctx,
        name=PARAMETER["name"],
        resource_id=PARAMETER["resource_id"],
    )
    assert ret["result"], ret["comment"]
    assert already_absent_message in ret["comment"]
    assert (not ret.get("old_state")) and (not ret.get("new_state"))
    if not __test:
        PARAMETER.pop("resource_id")


@pytest.mark.asyncio
async def test_absent_with_none_resource_id(hub, ctx):
    temp_name = "idem-test-function-event-config-" + str(int(time.time()))
    # Delete with resource_id as None. Should result in no-op.
    ret = await hub.states.aws.lambda_aws.function_event_invoke_config.absent(
        ctx,
        name=temp_name,
        resource_id=None,
    )
    assert ret["result"], ret["comment"]
    assert (not ret["old_state"]) and (not ret["new_state"])
    already_absent_message = hub.tool.aws.comment_utils.already_absent_comment(
        resource_type="aws.lambda.function_event_invoke_config",
        name=temp_name,
    )[0]
    assert already_absent_message in ret["comment"]


@pytest_asyncio.fixture(scope="module")
async def cleanup(hub, ctx):
    global PARAMETER
    yield None
    if "resource_id" in PARAMETER:
        ret = await hub.states.aws.lambda_aws.function_event_invoke_config.absent(
            ctx,
            name=PARAMETER["name"],
            resource_id=PARAMETER["resource_id"],
        )
        assert ret["result"], ret["comment"]
