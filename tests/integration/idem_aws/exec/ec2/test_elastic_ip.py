import time
from typing import List

import pytest


@pytest.mark.asyncio
@pytest.mark.localstack(pro=False)
async def test_get(hub, ctx, aws_ec2_elastic_ip):
    elastic_ip_get_name = "idem-test-exec-get-elastic-ip-" + str(int(time.time()))
    tags_filter = {"tag:Name": aws_ec2_elastic_ip["tags"]["Name"]}
    ret = await hub.exec.aws.ec2.elastic_ip.get(
        ctx,
        name=elastic_ip_get_name,
        resource_id=aws_ec2_elastic_ip["resource_id"],
        tags=tags_filter,
    )
    assert ret["result"], ret["comment"]
    assert ret["ret"]
    resource = ret["ret"]
    assert elastic_ip_get_name == resource["name"]
    assert aws_ec2_elastic_ip["resource_id"] == resource["resource_id"]


@pytest.mark.asyncio
@pytest.mark.localstack(pro=False)
async def test_get_invalid_resource_id(hub, ctx):
    elastic_ip_get_name = "idem-test-exec-get-elastic-ip-" + str(int(time.time()))
    ret = await hub.exec.aws.ec2.elastic_ip.get(
        ctx,
        name=elastic_ip_get_name,
        resource_id="fake-id",
    )
    assert ret["result"], ret["comment"]
    assert ret["ret"] is None
    assert f"Get aws.ec2.elastic_ip '{elastic_ip_get_name}' result is empty" in str(
        ret["comment"]
    )


@pytest.mark.asyncio
@pytest.mark.localstack(pro=False)
async def test_list(hub, ctx, aws_ec2_elastic_ip):
    elastic_ip_list_name = "idem-test-exec-get-elastic-ip-" + str(int(time.time()))
    tags_filter = {"tag:Name": aws_ec2_elastic_ip["tags"]["Name"]}
    ret = await hub.exec.aws.ec2.elastic_ip.list(
        ctx, name=elastic_ip_list_name, tags=tags_filter
    )
    assert ret["result"], ret["comment"]
    assert isinstance(ret["ret"], List)
    assert len(ret["ret"]) == 1
    resource = ret["ret"][0]
    assert aws_ec2_elastic_ip["resource_id"] == resource.get("resource_id")
